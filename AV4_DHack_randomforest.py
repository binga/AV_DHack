import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier

train = pd.read_csv("train_FBFog7d.csv")
test = pd.read_csv("Test_L4P23N3.csv")
submission = pd.read_csv("Sample_Submission_i9bgj6r.csv")

alcohol = pd.read_csv("NewVariable_Alcohol.csv")
train = pd.merge(train, alcohol, how='inner')
test = pd.merge(test, alcohol, how='inner')

le = LabelEncoder()
target = train['Happy']
target = le.fit_transform(target)

test_ids = test['ID']
train.drop(['ID', 'Happy'], axis=1, inplace=True)
test.drop(['ID'], axis=1, inplace=True)

# Using only 5 variables.
train = train[['Alcohol_Consumption', 'Engagement_Religion', 'Var2', 'babies', 'Unemployed10']]
test = test[['Alcohol_Consumption', 'Engagement_Religion', 'Var2', 'babies', 'Unemployed10']]

train = pd.get_dummies(train)
test = pd.get_dummies(test)

train = train.fillna(0)
test = test.fillna(0)

clf = RandomForestClassifier(n_estimators=400, criterion='entropy',
                             min_samples_leaf=10, bootstrap=True,
                             n_jobs=-1, random_state=1234)

clf.fit(train, target)

test_preds = clf.predict(test)
test_preds = le.inverse_transform(np.array(test_preds, dtype=int))

submission['ID'] = test_ids
submission['Happy'] = test_preds
print submission['Happy'].value_counts()
submission.to_csv("final.csv", index=False)
