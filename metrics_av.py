import pandas as pd


def diff_to_score(diff):
    if diff == 0:
        return 50
    elif diff == 5:
        return 10
    elif diff == 10:
        return 5
    elif diff == -5:
        return -5
    elif diff == -10:
        return -10


def happy_to_scores(x):
    if x == 'Very Happy':
        return 15
    elif x == 'Pretty Happy':
        return 10
    elif x == 'Not Happy':
        return 5


def eval_metric(y_true, y_pred):
    scores = pd.DataFrame({'actual': y_true, 'predicted': y_pred})

    scores['actual'] = scores['actual'].apply(lambda x: happy_to_scores(x))
    scores['predicted'] = scores['predicted'].apply(lambda x: happy_to_scores(x))

    # scores['actual'] = scores['actual'].astype(int)
    # scores['predicted'] = scores['predicted'].astype(int)
    scores['diff'] = scores['actual'] - scores['predicted']
    scores['scores'] = scores['diff'].map(lambda x: diff_to_score(x))

    final_score = sum(scores['scores']) / (float(len(scores)) * 50)
    return final_score

# test

a = ['Pretty Happy', 'Very Happy', 'Not Happy']
b = ['Very Happy', 'Very Happy', 'Very Happy']
print eval_metric(a, b)
