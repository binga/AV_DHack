import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.preprocessing import LabelEncoder

from metrics_av import *

train = pd.read_csv("input/train_FBFog7d.csv")
test = pd.read_csv("input/Test_L4P23N3.csv")
submission = pd.read_csv("input/Sample_Submission_i9bgj6r.csv")

alcohol = pd.read_csv("input/NewVariable_Alcohol.csv")
train = pd.merge(train, alcohol, how='inner')
test = pd.merge(test, alcohol, how='inner')


def eval_metric_sklearn(y_true, y_pred):
    y_pred = le.inverse_transform(np.array(y_pred, dtype='int'))
    y_true = le.inverse_transform(np.array(y_true, dtype='int'))
    score = eval_metric(y_true, y_pred)
    return score


def eval_metric_xgb(predicted, dtrain):
    actual = dtrain.get_label()

    predicted = le.inverse_transform(np.array(predicted, dtype='int'))
    actual = le.inverse_transform(np.array(actual, dtype='int'))
    score = eval_metric(actual, predicted)
    return 'metric', score


# Preprocessing
train.ix[pd.isnull(train['Var1']), 'Var1'] = 'G'
train.ix[pd.isnull(train['WorkStatus']), 'WorkStatus'] = 'working fulltime'
train.ix[pd.isnull(train['Divorce']), 'Divorce'] = '-1'

test.ix[pd.isnull(test['Var1']), 'Var1'] = 'G'
test.ix[pd.isnull(test['Divorce']), 'Divorce'] = '-1'

print train.dtypes
le = LabelEncoder()
target = train['Happy']
target = le.fit_transform(target)

test_ids, train_ids = test['ID'], train['ID']
train.drop(['ID', 'Happy'], axis=1, inplace=True)
test.drop(['ID'], axis=1, inplace=True)

train = pd.get_dummies(train)
test = pd.get_dummies(test)

feat = [x for x in train.columns.values if 'Alcohol' in x]
feat1 = [x for x in train.columns.values if 'Engagement_Religion' in x]
feat = feat + feat1
print feat

dtrain = xgb.DMatrix(train[feat].values, target, missing=np.nan)
dtest = xgb.DMatrix(test[feat].values, missing=np.nan)

params = {'booster': 'gbtree', 'objective': 'multi:softmax', 'nthread': 4,
          'max_depth': 4, 'eta': 0.1, 'silent': 1, 'subsample': 0.8,
          'colsample_bytree': 0.8, 'min_child_weight': 1, 'max_delta_step': 1,
          'num_class': 3}
num_rounds = 200

params['seed'] = 0
# xgb.cv(params, dtrain, num_rounds, nfold=4, show_progress=True)#, feval=eval_metric_xgb)
# exit()
bst = xgb.train(params, dtrain, num_rounds)

test_preds = bst.predict(dtest)
test_preds = le.inverse_transform(np.array(test_preds, dtype=int))

submission['ID'] = test_ids
submission['Happy'] = test_preds
print submission['Happy'].value_counts()
submission.to_csv("Submissions/v8.csv", index=False)
